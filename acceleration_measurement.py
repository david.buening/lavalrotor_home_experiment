import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "/home/pi/lavalrotor_home_experiment/datasheets/setup_mixer_1eeaa401-ad33-62e3-9dea-4a7b4fe21548.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Dictionary erstellen
beschleunigung_werte = {'1ee847be-fddd-6ee4-892a-68c4555b0981': {
                       'acceleration_x': [],
                       'acceleration_y': [],
                       'acceleration_z': [],
                       'timestamp': [],
                       }
}
# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
startzeit = time.time()

while time.time() - startzeit < measure_duration_in_s:

    acc_werte = accelerometer.acceleration
    # Beschleunigungen in x, y, z Richtung in den jeweiligen Listen in "beschleunigung_werte" anhängen
    beschleunigung_werte['1ee847be-fddd-6ee4-892a-68c4555b0981']['acceleration_x'].append(acc_werte[0])
    beschleunigung_werte['1ee847be-fddd-6ee4-892a-68c4555b0981']['acceleration_y'].append(acc_werte[1])
    beschleunigung_werte['1ee847be-fddd-6ee4-892a-68c4555b0981']['acceleration_z'].append(acc_werte[2])
    # Zeitschritte abspeichern und in "beschleunigung_werte" bei "timestamps" anhängen
    zeit = time.time() - startzeit
    beschleunigung_werte['1ee847be-fddd-6ee4-892a-68c4555b0981']['timestamp'].append(zeit)
    # 1 ms warten
    time.sleep(0.001)

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# data_20240104_124420_YourNameWithoutWhiteSpaces_Handy.h5
print(beschleunigung_werte["1ee847be-fddd-6ee4-892a-68c4555b0981"])
with h5py.File(path_h5_file, "w") as f:
    # Gruppe RawData erstellen
    grp_raw = f.create_group("RawData")
    # Gruppe mit UUID des Beschleunigungssensors erstellen
    grp_sensor = grp_raw.create_group("1ee847be-fddd-6ee4-892a-68c4555b0981")
    # Schleife mit allen Listen in "beschleunigung_werte" durchgehen
    for daten in beschleunigung_werte["1ee847be-fddd-6ee4-892a-68c4555b0981"]:
        # Dataset für alle Listen aus beschleunigung_werte erstellen
        datei = grp_sensor.create_dataset(daten, data = beschleunigung_werte["1ee847be-fddd-6ee4-892a-68c4555b0981"][daten])
        # Attribute für timestamp und alle Beschleunigungen festlegen
        if daten == "timestamp":
            datei.attrs["unit"] = "s"       
            
        else:
            datei.attrs["unit"] = "m/s²"     

# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
